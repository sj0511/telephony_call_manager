/*
 * Copyright (C) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CALL_MANAGER_INNER_TYPE_H
#define CALL_MANAGER_INNER_TYPE_H

#include <cstdio>
#include <string>
#include <vector>
#include <ctime>

namespace OHOS {
namespace Telephony {
constexpr int16_t kMaxNumberLen = 100;
constexpr int16_t kMaxBundleNameLen = 100;
constexpr int16_t kMaxAddressLen = 100;
constexpr uint16_t REJECT_CALL_MSG_MAX_LEN = 300;
constexpr uint16_t ACCOUNT_NUMBER_MAX_LENGTH = 100;
constexpr uint16_t CONNECT_SERVICE_WAIT_TIME = 1000; // ms
constexpr int16_t ERR_ID = -1;
constexpr int16_t INVALID_CALLID = 0;
constexpr int16_t WAIT_TIME_ONE_SECOND = 1;
// The follow hour and minute was use to confirm the set call transfer info time
constexpr int16_t MIN_HOUR = 0;
constexpr int16_t MAX_HOUR = 24;
constexpr int16_t MIN_MINUTE = 0;
constexpr int16_t MAX_MINUTE = 60;
constexpr int16_t INVALID_TIME = -1;

// call type
enum class CallType {
    TYPE_CS = 0, // CS
    TYPE_IMS = 1, // IMS
    TYPE_OTT = 2, // OTT
    TYPE_ERR_CALL = 3, // OTHER
};

// call state
enum class TelCallState {
    CALL_STATUS_UNKNOWN = -1,
    CALL_STATUS_ACTIVE = 0,
    CALL_STATUS_HOLDING,
    CALL_STATUS_DIALING,
    CALL_STATUS_ALERTING,
    CALL_STATUS_INCOMING,
    CALL_STATUS_WAITING,
    CALL_STATUS_DISCONNECTED,
    CALL_STATUS_DISCONNECTING,
    CALL_STATUS_IDLE,
};

enum class TelConferenceState {
    TEL_CONFERENCE_IDLE = 0,
    TEL_CONFERENCE_ACTIVE,
    TEL_CONFERENCE_HOLDING,
    TEL_CONFERENCE_DISCONNECTING,
    TEL_CONFERENCE_DISCONNECTED,
};

// phone type
enum class PhoneNetType {
    PHONE_TYPE_GSM = 1, // gsm
    PHONE_TYPE_CDMA = 2, // cdma
};

// call mode
enum class VideoStateType {
    TYPE_VOICE = 0, // Voice
    TYPE_VIDEO, // Video
};

enum class DialScene {
    CALL_NORMAL = 0,
    CALL_PRIVILEGED,
    CALL_EMERGENCY,
};

enum class CallDirection {
    CALL_DIRECTION_IN = 0,
    CALL_DIRECTION_OUT,
    CALL_DIRECTION_UNKNOW,
};

enum class CallRunningState {
    CALL_RUNNING_STATE_CREATE = 0, // A new session
    CALL_RUNNING_STATE_CONNECTING,
    CALL_RUNNING_STATE_DIALING,
    CALL_RUNNING_STATE_RINGING,
    CALL_RUNNING_STATE_ACTIVE,
    CALL_RUNNING_STATE_HOLD,
    CALL_RUNNING_STATE_ENDED,
    CALL_RUNNING_STATE_ENDING,
};

enum class CallEndedType {
    UNKNOWN = 0,
    PHONE_IS_BUSY,
    INVALID_NUMBER,
    CALL_ENDED_NORMALLY,
};

struct SIMCardInfo {
    int32_t simId = 0; // IccId
    int32_t country = 0;
    int32_t state = 0; // SIM card active status
    PhoneNetType phoneNetType = PhoneNetType::PHONE_TYPE_GSM;
};

enum class DialType {
    DIAL_CARRIER_TYPE = 0,
    DIAL_VOICE_MAIL_TYPE,
    DIAL_OTT_TYPE,
};

enum class CallStateToApp {
    /**
     * Indicates an invalid state, which is used when the call state fails to be obtained.
     */
    CALL_STATE_UNKNOWN = -1,

    /**
     * Indicates that there is no ongoing call.
     */
    CALL_STATE_IDLE = 0,

    /**
     * Indicates that an incoming call is ringing or waiting.
     */
    CALL_STATE_RINGING = 1,

    /**
     * Indicates that a least one call is in the dialing, active, or hold state, and there is no new incoming call
     * ringing or waiting.
     */
    CALL_STATE_OFFHOOK = 2
};

enum class CallAnswerType {
    CALL_ANSWER_MISSED = 0,
    CALL_ANSWER_ACTIVED,
    CALL_ANSWER_REJECT,
};

struct CallAttributeInfo {
    char accountNumber[kMaxNumberLen + 1] = { 0 };
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
    bool speakerphoneOn = false;
    int32_t accountId = 0;
    VideoStateType videoState = VideoStateType::TYPE_VOICE;
    int64_t startTime = 0; // Call start time
    bool isEcc = false;
    CallType callType = CallType::TYPE_ERR_CALL;
    int32_t callId = 0;
    TelCallState callState = TelCallState::CALL_STATUS_UNKNOWN;
    TelConferenceState conferenceState = TelConferenceState::TEL_CONFERENCE_IDLE;
    time_t callBeginTime = 0;
    time_t callEndTime = 0;
    time_t ringBeginTime = 0;
    time_t ringEndTime = 0;
    CallDirection callDirection = CallDirection::CALL_DIRECTION_UNKNOW;
    CallAnswerType answerType = CallAnswerType::CALL_ANSWER_MISSED;
};

struct CallRecordInfo {
    int32_t callId = 0;
    char phoneNumber[kMaxNumberLen + 1] = { 0 };
    char formattedPhoneNumber[kMaxNumberLen + 1] = { 0 };
    CallType callType = CallType::TYPE_ERR_CALL;
    time_t callBeginTime = 0;
    time_t callEndTime = 0;
    uint32_t ringDuration = 0;
    uint32_t callDuration = 0;
    CallDirection directionType = CallDirection::CALL_DIRECTION_UNKNOW;
    CallAnswerType answerType = CallAnswerType::CALL_ANSWER_MISSED;
    int32_t countryCode = 0;
    int32_t slotId = 0;
};

enum class CallAbilityEventId {
    EVENT_DIAL_NO_CARRIER = 1,
    EVENT_INVALID_FDN_NUMBER,
    EVENT_OTT_FUNCTION_UNSUPPORTED,
};

struct CallEventInfo {
    CallAbilityEventId eventId = CallAbilityEventId::EVENT_DIAL_NO_CARRIER;
    char phoneNum[kMaxNumberLen + 1] = { 0 };
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
};

struct AccountInfo {
    int32_t accountId = 0;
    int32_t power = 0;
    char bundleName[kMaxNumberLen + 1] = { 0 };
    bool isEnabled = false;
};

struct CallReportInfo {
    int32_t index = 0;
    char accountNum[kMaxNumberLen + 1] = { 0 }; // call phone number
    int32_t accountId = 0;
    CallType callType = CallType::TYPE_ERR_CALL; // call type: CS、IMS
    VideoStateType callMode = VideoStateType::TYPE_VOICE; // call mode: video or audio
    TelCallState state = TelCallState::CALL_STATUS_UNKNOWN; // call state
    int32_t voiceDomain = 0; // 0:CS、1:IMS
};

struct CallsReportInfo {
    std::vector<CallReportInfo> callVec {};
    int32_t slotId = 0;
};

/*
 * 3GPP TS 24.008
 * V17.4.0 10.5.4.11 Cause
 * The purpose of the cause information element is to describe the reason for generating
 * certain messages, to provide diagnostic information in the event of procedural
 * errors and to indicate the location of the cause originator.
 */
enum class DisconnectedReason : int32_t {
    UNASSIGNED_NUMBER = 1,
    NO_ROUTE_TO_DESTINATION = 3,
    CHANNEL_UNACCEPTABLE = 6,
    OPERATOR_DETERMINED_BARRING = 8,
    CALL_COMPLETED_ELSEWHERE = 13,
    NORMAL_CALL_CLEARING = 16,
    USER_BUSY = 17,
    NO_USER_RESPONDING = 18,
    USER_ALERTING_NO_ANSWER = 19,
    CALL_REJECTED = 21,
    NUMBER_CHANGED = 22,
    CALL_REJECTED_DUE_TO_FEATURE_AT_THE_DESTINATION = 24,
    FAILED_PRE_EMPTION = 25,
    NON_SELECTED_USER_CLEARING = 26,
    DESTINATION_OUT_OF_ORDER = 27,
    INVALID_NUMBER_FORMAT = 28,
    FACILITY_REJECTED = 29,
    RESPONSE_TO_STATUS_ENQUIRY = 30,
    NORMAL_UNSPECIFIED = 31,
    NO_CIRCUIT_CHANNEL_AVAILABLE = 34,
    NETWORK_OUT_OF_ORDER = 38,
    TEMPORARY_FAILURE = 41,
    SWITCHING_EQUIPMENT_CONGESTION = 42,
    ACCESS_INFORMATION_DISCARDED = 43,
    REQUEST_CIRCUIT_CHANNEL_NOT_AVAILABLE = 44,
    RESOURCES_UNAVAILABLE_UNSPECIFIED = 47,
    QUALITY_OF_SERVICE_UNAVAILABLE = 49,
    REQUESTED_FACILITY_NOT_SUBSCRIBED = 50,
    INCOMING_CALLS_BARRED_WITHIN_THE_CUG = 55,
    BEARER_CAPABILITY_NOT_AUTHORIZED = 57,
    BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE = 58,
    SERVICE_OR_OPTION_NOT_AVAILABLE_UNSPECIFIED = 63,
    BEARER_SERVICE_NOT_IMPLEMENTED = 65,
    ACM_EQUALTO_OR_GREATE_THAN_ACMMAX = 68,
    REQUESTED_FACILITY_NOT_IMPLEMENTED = 69,
    ONLY_RESTRICTED_DIGITAL_INFO_BEARER_CAPABILITY_IS_AVAILABLE = 70,
    SERVICE_OR_OPTION_NOT_IMPLEMENTED_UNSPECIFIED = 79,
    INVALID_TRANSACTION_IDENTIFIER_VALUE = 81,
    USER_NOT_MEMBER_OF_CUG = 87,
    INCOMPATIBLE_DESTINATION = 88,
    INVALID_TRANSIT_NETWORK_SELECTION = 91,
    SEMANTICALLY_INCORRECT_MESSAGE = 95,
    INVALID_MANDATORY_INFORMATION = 96,
    MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED = 97,
    MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE = 98,
    INFORMATION_ELEMENT_NON_EXISTENT_OR_NOT_IMPLEMENTED = 99,
    CONDITIONAL_IE_ERROR = 100,
    MESSAGE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE = 101,
    RECOVERY_ON_TIMER_EXPIRED = 102,
    PROTOCOL_ERROR_UNSPECIFIED = 111,
    INTERWORKING_UNSPECIFIED = 127,
    CALL_BARRED = 240,
    FDN_BLOCKED = 241,
    IMSI_UNKNOWN_IN_VLR = 242,
    IMEI_NOT_ACCEPTED = 243,
    DIAL_MODIFIED_TO_USSD = 244, // STK Call Control
    DIAL_MODIFIED_TO_SS = 245,
    DIAL_MODIFIED_TO_DIAL = 246,
    RADIO_OFF = 247, // Radio is OFF
    OUT_OF_SERVICE = 248, // No cellular coverage
    NO_VALID_SIM = 249, // No valid SIM is present
    RADIO_INTERNAL_ERROR = 250, // Internal error at Modem
    NETWORK_RESP_TIMEOUT = 251, // No response from network
    NETWORK_REJECT = 252, // Explicit network reject
    RADIO_ACCESS_FAILURE = 253, // RRC connection failure. Eg.RACH
    RADIO_LINK_FAILURE = 254, // Radio Link Failure
    RADIO_LINK_LOST = 255, // Radio link lost due to poor coverage
    RADIO_UPLINK_FAILURE = 256, // Radio uplink failure
    RADIO_SETUP_FAILURE = 257, // RRC connection setup failure
    RADIO_RELEASE_NORMAL = 258, // RRC connection release, normal
    RADIO_RELEASE_ABNORMAL = 259, // RRC connection release, abnormal
    ACCESS_CLASS_BLOCKED = 260, // Access class barring
    NETWORK_DETACH = 261, // Explicit network detach
    INVALID_PARAMETER = 1025,
    SIM_NOT_EXIT = 1026,
    SIM_PIN_NEED = 1027,
    CALL_NOT_ALLOW = 1029,
    SIM_INVALID = 1045,
    FAILED_UNKNOWN = 1279,
};

struct DisconnectedDetails {
    DisconnectedReason reason = DisconnectedReason::FAILED_UNKNOWN;
    std::string message = "";
};

enum class AudioDeviceType {
    DEVICE_EARPIECE = 0,
    DEVICE_SPEAKER,
    DEVICE_WIRED_HEADSET,
    DEVICE_BLUETOOTH_SCO,
    DEVICE_DISABLE,
    DEVICE_UNKNOWN,
};

enum class CellularCallEventType {
    EVENT_REQUEST_RESULT_TYPE = 0,
};

enum class RequestResultEventId {
    INVALID_REQUEST_RESULT_EVENT_ID = -1,
    RESULT_DIAL_SEND_FAILED = 0,
    RESULT_DIAL_NO_CARRIER,
    RESULT_END_SEND_FAILED,
    RESULT_REJECT_SEND_FAILED,
    RESULT_ACCEPT_SEND_FAILED,
    RESULT_HOLD_SEND_FAILED,
    RESULT_ACTIVE_SEND_FAILED,
    RESULT_SWAP_SEND_FAILED,
    RESULT_JOIN_SEND_FAILED,
    RESULT_SPLIT_SEND_FAILED,
    RESULT_SUPPLEMENT_SEND_FAILED,
    RESULT_INVITE_TO_CONFERENCE_SUCCESS,
    RESULT_INVITE_TO_CONFERENCE_FAILED,
    RESULT_KICK_OUT_FROM_CONFERENCE_SUCCESS,
    RESULT_KICK_OUT_FROM_CONFERENCE_FAILED,

    RESULT_SEND_DTMF_SUCCESS,
    RESULT_SEND_DTMF_FAILED,

    RESULT_GET_CURRENT_CALLS_FAILED,

    RESULT_SET_CALL_PREFERENCE_MODE_SUCCESS,
    RESULT_SET_CALL_PREFERENCE_MODE_FAILED,
    RESULT_GET_IMS_CALLS_DATA_FAILED,

    RESULT_GET_CALL_WAITING_SUCCESS,
    RESULT_GET_CALL_WAITING_FAILED,
    RESULT_SET_CALL_WAITING_SUCCESS,
    RESULT_SET_CALL_WAITING_FAILED,
    RESULT_GET_CALL_RESTRICTION_SUCCESS,
    RESULT_GET_CALL_RESTRICTION_FAILED,
    RESULT_SET_CALL_RESTRICTION_SUCCESS,
    RESULT_SET_CALL_RESTRICTION_FAILED,
    RESULT_GET_CALL_TRANSFER_SUCCESS,
    RESULT_GET_CALL_TRANSFER_FAILED,
    RESULT_SET_CALL_TRANSFER_SUCCESS,
    RESULT_SET_CALL_TRANSFER_FAILED,
    RESULT_SEND_USSD_SUCCESS,
    RESULT_SEND_USSD_FAILED,

    RESULT_SET_MUTE_SUCCESS,
    RESULT_SET_MUTE_FAILED,

    RESULT_CTRL_CAMERA_SUCCESS,
    RESULT_CTRL_CAMERA_FAILED,
    RESULT_SET_PREVIEW_WINDOW_SUCCESS,
    RESULT_SET_PREVIEW_WINDOW_FAILED,
    RESULT_SET_DISPLAY_WINDOW_SUCCESS,
    RESULT_SET_DISPLAY_WINDOW_FAILED,
    RESULT_SET_CAMERA_ZOOM_SUCCESS,
    RESULT_SET_CAMERA_ZOOM_FAILED,
    RESULT_SET_PAUSE_IMAGE_SUCCESS,
    RESULT_SET_PAUSE_IMAGE_FAILED,
    RESULT_SET_DEVICE_DIRECTION_SUCCESS,
    RESULT_SET_DEVICE_DIRECTION_FAILED,
};

enum class CallResultReportId {
    START_DTMF_REPORT_ID = 0,
    STOP_DTMF_REPORT_ID,
    SEND_USSD_REPORT_ID,
    GET_IMS_CALL_DATA_REPORT_ID,
    GET_CALL_WAITING_REPORT_ID,
    SET_CALL_WAITING_REPORT_ID,
    GET_CALL_RESTRICTION_REPORT_ID,
    SET_CALL_RESTRICTION_REPORT_ID,
    GET_CALL_TRANSFER_REPORT_ID,
    SET_CALL_TRANSFER_REPORT_ID,
    GET_CALL_CLIP_ID,
    GET_CALL_CLIR_ID,
    SET_CALL_CLIR_ID,
    START_RTT_REPORT_ID,
    STOP_RTT_REPORT_ID,
    GET_IMS_CONFIG_REPORT_ID,
    SET_IMS_CONFIG_REPORT_ID,
    GET_IMS_FEATURE_VALUE_REPORT_ID,
    SET_IMS_FEATURE_VALUE_REPORT_ID,
    INVITE_TO_CONFERENCE_REPORT_ID,
    UPDATE_MEDIA_MODE_REPORT_ID,
    CLOSE_UNFINISHED_USSD_REPORT_ID,
};

struct CellularCallEventInfo {
    CellularCallEventType eventType = CellularCallEventType::EVENT_REQUEST_RESULT_TYPE;
    RequestResultEventId eventId = RequestResultEventId::INVALID_REQUEST_RESULT_EVENT_ID;
};

enum class RBTPlayInfo {
    NETWORK_ALERTING,
    LOCAL_ALERTING,
};

struct CallWaitResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t status = 0;
    int32_t classCw = 0;
};

struct ClipResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t action = 0;
    int32_t clipStat = 0;
};

struct ClirResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t action = 0;
    int32_t clirStat = 0;
};

struct ColrResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t action = 0;
    int32_t colrStat = 0;
};

struct ColpResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t action = 0;
    int32_t colpStat = 0;
};

struct MmiCodeInfo {
    int32_t result = 0; // 0: ok  other: error
    char message[kMaxNumberLen + 1] = { 0 };
};

struct CallTransferResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t status = 0;
    int32_t classx = 0;
    int32_t type = 0;
    char number[kMaxNumberLen + 1] = { 0 };
    int32_t reason = 0;
    int32_t time = 0;
    int32_t startHour = INVALID_TIME;
    int32_t startMinute = INVALID_TIME;
    int32_t endHour = INVALID_TIME;
    int32_t endMinute = INVALID_TIME;
};

struct CallRestrictionResponse {
    int32_t result = 0; // 0: ok  other: error
    int32_t status = 0; // parameter sets/shows the result code presentation status in the TA
    int32_t classCw = 0; // parameter shows the subscriber CLIP service status in the network, <0-4>
};

struct CallPreferenceResponse {
    int32_t result = 0; // 0: ok  other: error
    /*
     * 1：CS Voice only
     * 2：CS Voice preferred, IMS PS Voice as secondary
     * 3：IMS PS Voice preferred, CS Voice as secondary
     * 4：IMS PS Voice only
     */
    int32_t mode = 0;
};

struct GetImsConfigResponse {
    int32_t result = 0;
    int32_t value = 0;
};

struct GetImsFeatureValueResponse {
    int32_t result = 0;
    int32_t value = 0;
};

struct GetLteEnhanceModeResponse {
    int32_t result = 0;
    int32_t value = 0;
};

struct MuteControlResponse {
    int32_t result = 0;
    int32_t value = 0; // 0: Un mute 1: Set mute
};

struct CellularCallInfo {
    int32_t callId = 0; // uuid
    char phoneNum[kMaxNumberLen] = { 0 }; // call phone number
    int32_t slotId = 0; // del
    int32_t accountId = 0;
    CallType callType = CallType::TYPE_ERR_CALL; // call type: CS、IMS
    int32_t videoState = 0; // 0: audio 1:video
    int32_t index = 0; // CallInfo index
};

/**
 * 27007-430_2001 7.11	Call forwarding number and conditions +CCFC
 * 3GPP TS 22.082 [4]
 * <mode>:
 * 0	disable
 * 1	enable
 * 3	registration
 * 4	erasure
 */
enum class CallTransferSettingType {
    CALL_TRANSFER_DISABLE = 0,
    CALL_TRANSFER_ENABLE = 1,
    CALL_TRANSFER_REGISTRATION = 3,
    CALL_TRANSFER_ERASURE = 4,
};

/**
 * 27007-430_2001 7.11	Call forwarding number and conditions +CCFC
 * 3GPP TS 22.082 [4]
 * <reason>:
 * 0	unconditional
 * 1	mobile busy
 * 2	no reply
 * 3	not reachable
 */
enum class CallTransferType {
    TRANSFER_TYPE_UNCONDITIONAL = 0,
    TRANSFER_TYPE_BUSY = 1,
    TRANSFER_TYPE_NO_REPLY = 2,
    TRANSFER_TYPE_NOT_REACHABLE = 3,
};

struct CallTransferInfo {
    char transferNum[kMaxNumberLen + 1] = { 0 };
    CallTransferSettingType settingType = CallTransferSettingType::CALL_TRANSFER_DISABLE;
    CallTransferType type = CallTransferType::TRANSFER_TYPE_UNCONDITIONAL;
    int32_t startHour = 0;
    int32_t startMinute = 0;
    int32_t endHour = 0;
    int32_t endMinute = 0;
};

enum class SsRequestType {
    SS_ACTIVATION = 0,
    SS_DEACTIVATION,
    SS_INTERROGATION,
    SS_REGISTRATION,
    SS_ERASURE,
};

struct VideoWindow {
    int32_t x = 0;
    int32_t y = 0;
    int32_t z = 0;
    int32_t width = 0;
    int32_t height = 0;
};

// 3GPP TS 22.030 V4.0.0 (2001-03)
// 3GPP TS 22.088 V4.0.0 (2001-03)
enum class CallRestrictionType {
    RESTRICTION_TYPE_ALL_INCOMING = 0,
    RESTRICTION_TYPE_ALL_OUTGOING,
    RESTRICTION_TYPE_INTERNATIONAL,
    RESTRICTION_TYPE_INTERNATIONAL_EXCLUDING_HOME,
    RESTRICTION_TYPE_ROAMING_INCOMING,
    RESTRICTION_TYPE_ALL_CALLS,
    RESTRICTION_TYPE_OUTGOING_SERVICES,
    RESTRICTION_TYPE_INCOMING_SERVICES,
};

// 3GPP TS 22.088 V4.0.0 (2001-03)
enum class CallRestrictionMode {
    RESTRICTION_MODE_DEACTIVATION = 0,
    RESTRICTION_MODE_ACTIVATION = 1,
};

struct CallRestrictionInfo {
    char password[kMaxNumberLen + 1] = { 0 };
    CallRestrictionType fac = CallRestrictionType::RESTRICTION_TYPE_ALL_INCOMING;
    CallRestrictionMode mode = CallRestrictionMode::RESTRICTION_MODE_DEACTIVATION;
};

// 3GPP TS 27.007 V3.9.0 (2001-06) Call related supplementary services +CHLD
// 3GPP TS 27.007 V3.9.0 (2001-06) 7.22	Informative examples
enum CallSupplementType {
    TYPE_DEFAULT = 0, // default type
    TYPE_HANG_UP_HOLD_WAIT = 1, // release the held call and the wait call
    TYPE_HANG_UP_ACTIVE = 2, // release the active call and recover the held call
    TYPE_HANG_UP_ALL = 3, // release all calls
};

enum CellularCallReturnType {
    // 3GPP TS 27.007 V3.9.0 (2001-06) 6.27	Informative examples
    RETURN_TYPE_MMI = 0,
};

// 3GPP TS 27.007 V17.3.0 (2021-09) 10.1.35	UE's voice domain preference E-UTRAN +CEVDP
enum DomainPreferenceMode {
    CS_VOICE_ONLY = 1,
    CS_VOICE_PREFERRED = 2,
    IMS_PS_VOICE_PREFERRED = 3,
    IMS_PS_VOICE_ONLY = 4,
};

enum ImsCallMode {
    CALL_MODE_AUDIO_ONLY = 0,
    CALL_MODE_SEND_ONLY,
    CALL_MODE_RECEIVE_ONLY,
    CALL_MODE_SEND_RECEIVE,
    CALL_MODE_VIDEO_PAUSED,
};

struct CallMediaModeRequest {
    char phoneNum[kMaxNumberLen + 1] = { 0 };
    ImsCallMode mode = ImsCallMode::CALL_MODE_AUDIO_ONLY;
};

struct CallMediaModeResponse {
    char phoneNum[kMaxNumberLen + 1] = { 0 };
    int32_t result = 0;
};

enum ImsConfigItem {
    /**
     * video quality
     *
     * format: bool
     */
    ITEM_VIDEO_QUALITY = 0,

    /**
     * 3GPP TS 24.167 V17.1.0 (2020-12) 5.31 /<X>/Mobility_Management_IMS_Voice_Termination
     * Ims Switch Status
     *
     * format: bool
     * Access Types: Get, Replace
     * Values: 0, 1
     * 0 – Mobility Management for IMS Voice Termination disabled.
     * 1 – Mobility Management for IMS Voice Termination enabled.
     */
    ITEM_IMS_SWITCH_STATUS,
};

enum class VoNRState {
    /** Indicates the VoNR switch is on */
    VONR_STATE_ON = 0,
    /** Indicates the VoNR switch is off */
    VONR_STATE_OFF = 1,
};

enum VideoQualityValue {
    LOW = 0,
    HIGH = 1,
};

enum ImsConfigValue {
    CONFIG_SUCCESS = 0,
    CONFIG_FAILED = 1,
};

enum FeatureType {
    /**
     * Official Document IR.92 - IMS Profile for Voice and SMS
     * Annex C MNO provisioning and Late Customization
     *
     * format: bool
     * 3GPP TS 24.167 V17.1.0 (2020-12) 5.43 /<X>/Media_type_restriction_policy
     */
    TYPE_VOICE_OVER_LTE = 0,

    /**
     * Official Document IR.92 - IMS Profile for Voice and SMS
     * Annex C MNO provisioning and Late Customization
     *
     * format: bool
     * 3GPP TS 24.167 V17.1.0 (2020-12) 5.43 /<X>/Media_type_restriction_policy
     */
    TYPE_VIDEO_OVER_LTE,
    TYPE_SS_OVER_UT,
};

enum class OttCallRequestId {
    OTT_REQUEST_DIAL = 0,
    OTT_REQUEST_HANG_UP,
    OTT_REQUEST_REJECT,
    OTT_REQUEST_ANSWER,
    OTT_REQUEST_HOLD,
    OTT_REQUEST_UN_HOLD,
    OTT_REQUEST_SWITCH,
    OTT_REQUEST_COMBINE_CONFERENCE,
    OTT_REQUEST_SEPARATE_CONFERENCE,
    OTT_REQUEST_INVITE_TO_CONFERENCE,
    OTT_REQUEST_UPDATE_CALL_MEDIA_MODE,
};

struct OttCallRequestInfo {
    char phoneNum[kMaxNumberLen + 1] = { 0 }; // call phone number
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
    VideoStateType videoState = VideoStateType::TYPE_VOICE; // 0: audio 1:video
};

struct OttCallDetailsInfo {
    char phoneNum[kMaxNumberLen + 1] = { 0 }; // call phone number
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
    VideoStateType videoState = VideoStateType::TYPE_VOICE; // 0: audio 1:video
    TelCallState callState = TelCallState::CALL_STATUS_UNKNOWN;
};

enum class OttCallEventId {
    OTT_CALL_EVENT_FUNCTION_UNSUPPORTED = 0,
};

struct OttCallEventInfo {
    OttCallEventId ottCallEventId = OttCallEventId::OTT_CALL_EVENT_FUNCTION_UNSUPPORTED;
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
};

struct CallDetailInfo {
    int32_t index = 0;
    char phoneNum[kMaxNumberLen + 1] = { 0 }; // call phone number
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
    int32_t accountId = 0;
    CallType callType = CallType::TYPE_ERR_CALL; // call type: CS、IMS
    VideoStateType callMode = VideoStateType::TYPE_VOICE; // call mode: video or audio
    TelCallState state = TelCallState::CALL_STATUS_UNKNOWN; // call state
    int32_t voiceDomain = 0; // 0:CS、1:IMS
};

struct CallDetailsInfo {
    std::vector<CallDetailInfo> callVec {};
    int32_t slotId = 0;
    char bundleName[kMaxBundleNameLen + 1] = { 0 };
};

struct AudioDevice {
    AudioDeviceType deviceType = AudioDeviceType::DEVICE_UNKNOWN;
    char address[kMaxAddressLen + 1] = { 0 };
};

struct AudioDeviceInfo {
    std::vector<AudioDevice> audioDeviceList {};
    AudioDevice currentAudioDevice;
    bool isMuted = false;
};

enum class MmiCodeResult {
    MMI_CODE_SUCCESS = 0,
    MMI_CODE_FAILED = 1
};
} // namespace Telephony
} // namespace OHOS
#endif // CALL_MANAGER_INNER_TYPE_H
